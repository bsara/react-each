# 2.0.0

- **[BREAKING CHANGE]** Class syntax is now used without being transpiled.
- Updated some file names and converted to using `rollup` for build.
- Upgraded to latest versions of dev dependencies.



# 1.1.0

- **[NEW FEATURE]** ability to specify a `startIndex` and a `endIndex` such that only items that
  are within the resulting range will be rendered.


# 1.0.5

- Optimized package size by only including what is needed in npm package.
- Updated dependencies.


# 1.0.3

- Made license notices smaller.


# 1.0.2

- Updated dependencies


# 1.0.1

- Moved codebase to GitLab


# 1.0.0

- Initial release.
